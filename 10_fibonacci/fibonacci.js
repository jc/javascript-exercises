const fibonacci = function (num) {
	// num is the position of a number in an array made up of fibonacci numbers

	// the fibonacci sequence is n = (n-2) + (n-1)

	let fibArray = [0, 1];
	let val = parseInt(num);
	if (val < 0) return 'OOPS';

	for (let i = 2; i <= val; i++) {
		fibArray[i] = fibArray[i - 2] + fibArray[i - 1];
	}

	return fibArray[fibArray.length - 1]; //array positions start with 0, num is to be the last position which is the array length - 1
};

// Do not edit below this line
module.exports = fibonacci;
