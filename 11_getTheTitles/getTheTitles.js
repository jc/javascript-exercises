const getTheTitles = function (bookArray) {
	let newArray = bookArray.map((book) => book.title);

	return newArray;
};

// Do not edit below this line

module.exports = getTheTitles;
