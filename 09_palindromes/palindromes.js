const palindromes = function (string) {
	let splitArray = string
		.toLowerCase()
		.replace(/[^a-z]/g, '')
		.split('');
	return splitArray.join('') === splitArray.reverse().join('');
};

// Do not edit below this line
module.exports = palindromes;
