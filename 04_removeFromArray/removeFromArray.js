const removeFromArray = function (...args) {
	const oldArray = args[0];

	const newArray = [];

	oldArray.forEach(function (item) {
		if (!args.includes(item)) {
			newArray.push(item);
		}
	});

	// Written differently (no arrow function):

	// const includesItem = function (item) {
	// 	if (args.includes(item) === false) {
	// 		newArray.push(item);
	// 	}
	// };

	// oldArray.forEach(includesItem);

	return newArray;
};

// Do not edit below this line
module.exports = removeFromArray;
