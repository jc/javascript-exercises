const caesar = function (input, shift) {
	let alphabet = 'abcdefghijklmnopqrstuvwxyz';
	let upperAlphabet = alphabet.toUpperCase();

	let inputArray = input.split('');

	let noIdea = inputArray.map(function (item) {
		let inputPosition = alphabet.indexOf(item.toLowerCase());

		console.log(inputPosition);

		let outputPosition = (inputPosition + shift + 26) % 26;

		if (item === item.toUpperCase()) {
			return upperAlphabet[outputPosition];
		} else {
			return alphabet[outputPosition];
		}
	});

	return noIdea.join('');
};

console.log(caesar);

// let inputPosition = upperAlphabet.indexOf(input);

// let outputPosition = inputPosition + shift;

// return upperAlphabet[outputPosition];

// return    .join('');

// Do not edit below this line
module.exports = caesar;
