const add = function (num1, num2) {
	let sum = num1 + num2;
	return sum;
};

const subtract = function (num1, num2) {
	let sum = num1 - num2;
	return sum;
};

const sum = function ([...args]) {
	let myArray = [...args];
	let totalSum = myArray.reduce((first, next) => first + next, 0);
	return totalSum;
};

const multiply = function ([...args]) {
	let myArray = [...args];
	let totalSum = myArray.reduce((first, next) => first * next, 1);
	return totalSum;
};

const power = function (num1, num2) {
	let sum = num1 ** num2;
	return sum;
};

const factorial = function (val) {
	let result = 1;
	for (let i = 0; i < val; i++) {
		result = result * (val - i);
	}

	return result;
};

// Do not edit below this line
module.exports = {
	add,
	subtract,
	sum,
	multiply,
	power,
	factorial,
};
