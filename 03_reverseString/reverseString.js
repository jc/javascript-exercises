const reverseString = function (anyString) {
	return anyString.split('').reverse().join('');
};

// Do not edit below this line
module.exports = reverseString;
